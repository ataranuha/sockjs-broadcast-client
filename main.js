var _app = {
    user: {},
    connect: function () {
        if (this.sock) {
            this.sock.close
            this.sock = null;
        }
        var connectionWhitelist = [  //working source
            'websocket',
            'iframe-htmlfile',
            'xdr-polling',
            'xhr-polling',
            'xdr-streaming',
            'xhr-streaming',
            'iframe-eventsource',
            'iframe-xhr-polling',
            'jsonp-polling'
        ];

        var host = $('#host').val();
        this.sock = new SockJS(host, null, {
            //protocols_whitelist: connectionWhitelist
        });

        // on connection open
        this.sock.onopen = function () {
            var channels = _app.channels.split(" ");
            var msg = {channels: channels};
            _app.sock.send(JSON.stringify(msg));
            console.log('open: '+JSON.stringify(msg));
        };


        // on get message from server
        this.sock.onmessage = function (e) {
			var msg = window.unescape(e.data);
            console.log('in message: '+msg);
            $('#last_signal').html(msg);
        };

        // on connection close
        this.sock.onclose = function () {
            //reconnect
            this.reconnectTO = setTimeout(function () {
                console.log('reconnect');
                _app.connect();
            }, 2000);
            console.log('close');
        };
    }
};

$('#host').val("http://"+window.location.hostname+':8888/sockjs');
$('#post').val("http://"+window.location.hostname+':8888/post');

// stream connect/reconnect button
$('#btn-stream-connect').live('click', function () {
    _app.channels = $('#stream_chanel_id').val();
    _app.connect();
});

// post data button
$('#btn-send-start').live('click', function () {
    _app.send_chanel_id = $('#send_chanel_id').val();
    var data = $('#send_data').val();
    var postTo = $('#post').val();
    $.ajax({
        type: "POST",
        url: postTo+"?id="+_app.send_chanel_id,
        data: data,
        success: function(){ }
    });
    console.log('Send: '+_app.send_chanel_id+' data:'+data);
});




